package entity;

public class Person {
    private String surname;
    private String name;
    private String middleName;

    public Person(String surname, String name, String middleName) {
        this.surname = surname;
        this.name = name;
        this.middleName = middleName;
    }

    @Override
    public String toString() {
        return surname + " " + name + " " + middleName;
    }
}
