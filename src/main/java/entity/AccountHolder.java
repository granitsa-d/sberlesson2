package entity;

import java.util.Objects;

public class AccountHolder extends Person{

    private int cardNumber;
    private int accountNumber;
    private int balance;

    public AccountHolder(String surname, String name, String middleName, int cardNumber, int accountNumber, int balance) {
        super(surname, name, middleName);
        this.cardNumber = cardNumber;
        this.accountNumber = accountNumber;
        this.balance = balance;
    }

    public String getFullNameLowerCase() {
        return super.toString().toLowerCase();
    }

    public String getFullNameUpperCase() {
        return super.toString().toUpperCase();
    }

    public int getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }

    public int getCardNumber() {
        return cardNumber;
    }

    public int getAccountNumber() {
        return accountNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AccountHolder that = (AccountHolder) o;
        return cardNumber == that.cardNumber ||
                accountNumber == that.accountNumber;
    }

    @Override
    public int hashCode() {
        return Objects.hash(cardNumber, accountNumber);
    }
}
