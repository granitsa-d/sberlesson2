package entity;

public class Data {
    private int day;
    private int mounts;
    private int year;
    private int hours;
    private int minutes;
    private int seconds;

    public Data(int day, int mounts, int year, int hours, int minutes, int seconds) {
        this.day = day;
        this.mounts = mounts;
        this.year = year;
        this.hours = hours;
        this.minutes = minutes;
        this.seconds = seconds;
    }


    @Override
    public String toString() {
        String result = null;

        if (day < 10)
            result = "0" + day + "-";
        else
            result = result + day + "-";

        if (mounts < 10)
            result = result + "0" + mounts + "-";
        else
            result = result + mounts + "-";

        result = result + year + " ";

        if (hours < 10)
            result = result + "0" + hours + "-";
        else
            result = result + hours + "-";

        if (minutes < 10)
            result = result + "0" + minutes + "-";
        else
            result = result + minutes + "-";

        if (seconds < 10)
            result = result + "0" + seconds;
        else
            result = result + seconds;

        return result;
    }
}
