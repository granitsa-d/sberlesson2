package entity;

public enum Currency {
    DOLLAR("ДОЛЛАР");

    private String title;

    Currency(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }
}
