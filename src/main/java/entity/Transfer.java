package entity;

public class Transfer {
    private int numberTranslation;
    private int amount;
    private Currency currency;
    private AccountHolder recipient;
    private AccountHolder sender;
    private Data data;

    public Transfer(int numberTranslation, int money, Currency currency
            , AccountHolder recipient, AccountHolder sender, Data date) {
        if (recipient.equals(sender)) {
            throw new IllegalArgumentException("currency and recipient is same person");
        }
        this.numberTranslation = numberTranslation;
        this.amount = money;
        this.currency = currency;
        this.recipient = recipient;
        this.sender = sender;
        this.data = date;
    }

    public void doTransferAccount() {
        if (sender.getBalance() > amount) {
            sender.setBalance(sender.getBalance() - amount);
            recipient.setBalance(recipient.getBalance() + amount);
            System.out.println("ПЕРЕВОД СО СЧЕТА НА СЧЕТ: " + '\n' +
                    "Номер перевода: " + numberTranslation + "," + '\n' +
                    "Дата: " + data.toString() + "," + '\n' +
                    "Номер счета получателя: " + recipient.getAccountNumber() + "," + '\n' +
                    "Номер счета отправителя: " + sender.getAccountNumber() + "," + '\n' +
                    "Валюта: " + currency.getTitle() + "," + '\n' +
                    "Сумма: " + amount + "," + '\n' +
                    "Получатель: " + recipient.toString() + "," + '\n' +
                    "Отправитель: " + sender.toString());
        } else {
            System.out.println("Недостаточно средств");
        }
    }

    public void doTransferCard() {
        if (sender.getBalance() > amount) {

            sender.setBalance(sender.getBalance() - amount);
            recipient.setBalance(recipient.getBalance() + amount);

            System.out.println("ПЕРЕВОД С КАРТЫ НА КАРТУ: " + '\n' +
                    "Номер перевода: " + numberTranslation + "," + '\n' +
                    "Дата: " + data.toString() + "," + '\n' +
                    "Номер карты получателя: " + recipient.getCardNumber() + "," + '\n' +
                    "Номер карты отправителя: " + sender.getCardNumber() + "," + '\n' +
                    "Валюта: " + currency.getTitle() + "," + '\n' +
                    "Сумма: " + amount + "," + '\n' +
                    "Получатель: " + recipient.toString() + "," + '\n' +
                    "Отправитель: " + sender.toString());
        } else {
            System.out.println("Недостаточно средств");
        }
    }
}
