import entity.AccountHolder;
import entity.Currency;
import entity.Data;
import entity.Transfer;

public class Main {

    public static void main(String[] args) {
        AccountHolder sender =
                new AccountHolder("Граница"
                        , "Дмитрий"
                        , "Игоревич"
                        , 123
                        , 123
                        , 53453);

        AccountHolder recipient =
                new AccountHolder("Федотов"
                        , "Алексадр"
                        , "Сергеевич"
                        , 435
                        , 234
                        , 53453);

        System.out.println(recipient.getFullNameLowerCase());
        System.out.println(recipient.getFullNameUpperCase());
        System.out.println('\n' + "*****************************" + '\n');

        Transfer transfer =
                new Transfer(222
                ,456
                ,Currency.DOLLAR
                ,recipient
                ,sender
                ,new Data(6,4,2020,15,2,12));

        transfer.doTransferAccount();
        System.out.println('\n' + "*****************************" + '\n');
        transfer.doTransferCard();
    }

}
